import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.less";
import AppLayout from "./components/AppLayout";
import { FetchProvider } from "./context/FetchContext";
import { LoadingProvider } from "./context/LoadingContext";
import { AuthProvider } from "./context/AuthContext";
import { createBrowserHistory as createHistory } from "history";

const history = createHistory();

const App = () => (
  <Router history={history}>
    <AuthProvider>
      <LoadingProvider>
        <FetchProvider>
          <AppLayout />
        </FetchProvider>
      </LoadingProvider>
    </AuthProvider>
  </Router>
);

export default App;

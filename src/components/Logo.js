import React from "react";

export const CircleLogo = () => (
  <img
    alt="logo"
    src={`${process.env.PUBLIC_URL}/slowkim.png`}
    width="30px"
    style={{
      borderRadius: "50%",
    }}
  />
);

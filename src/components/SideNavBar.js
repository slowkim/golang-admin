import React, { useState, useMemo } from "react";
import { NavigationConfig } from "../config/NavigationConfig";
import { Link, useHistory } from "react-router-dom";
import { Layout, Menu } from "antd";
import * as AllIcons from "@ant-design/icons";
import { CircleLogo } from "./Logo";
import { PoweroffOutlined } from "@ant-design/icons";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";

const MenuLink = ({ title, link }) => (
  <Link to={link}>
    <span>{title}</span>
  </Link>
);

const SideNavBar = ({ onMenuClick, userPermission, onLoggedOut }) => {
  const [collapsed, setCollapsed] = useState(true);
  const [selectedMenu, setSelectedMenu] = useState([]);
  const breakpoint = useBreakpoint();
  const history = useHistory();

  const handleMenuChange = () => {
    const currentPath = history.location.pathname;
    if (history.location.pathname === "/") {
      onMenuClick([]);
      setSelectedMenu([]);
      return;
    }
    const matched = NavigationConfig.FindItem(currentPath, userPermission);

    if (!matched) {
      return;
    }
    onMenuClick(matched);
    setSelectedMenu(matched);
  };

  useMemo(() => {
    const currentScreen = Object.entries(breakpoint)
      .filter((screen) => Boolean(screen[1]))
      .reverse()
      .map((screen) => screen[0])[0];

    if (["xs", "sm", "md"].includes(currentScreen)) {
      setCollapsed(true);
    } else {
      setCollapsed(false);
    }
  }, [breakpoint]);

  useMemo(() => {
    handleMenuChange();
    // navigation 탐색 후 setMenuDepth
    // 1. items를 갖거나 link를 갖거나 둘중 하나
    // 2. Gnb는 link를 가질 수 없다. (items만 가능)
  }, []);

  return (
    <Layout.Sider trigger={null} collapsible collapsed={collapsed}>
      {/* for Re-Render */}
      <Link to="/" onClick={handleMenuChange}>
        <div className="navbar-header">
          <CircleLogo />
          {!collapsed && <span>Admin</span>}
        </div>
      </Link>
      <Menu
        theme="dark"
        mode="inline"
        onClick={handleMenuChange}
        defaultOpenKeys={
          collapsed
            ? []
            : [
                selectedMenu
                  .filter((_, i) => i < selectedMenu.length - 1)
                  .map((menu) => String(menu.index))
                  .join("-"),
              ]
        }
        defaultSelectedKeys={
          collapsed ? [] : [selectedMenu.map((menu) => menu.index).join("-")]
        }
      >
        {NavigationConfig.FilterPermitted(
          NavigationConfig.Items,
          userPermission
        ).map((item) => {
          const SnbIcon = AllIcons[item.icon];
          if (item.link) {
            return (
              <Menu.Item key={item.index} icon={<SnbIcon />}>
                <MenuLink title={item.title} link={item.link} />
              </Menu.Item>
            );
          }
          return (
            <Menu.SubMenu
              key={item.index}
              title={item.title}
              icon={<SnbIcon />}
            >
              {NavigationConfig.FilterPermitted(item.items, userPermission).map(
                (subItem) => {
                  const SubItemIcon = AllIcons[subItem.icon];
                  return (
                    <Menu.Item key={subItem.index} icon={<SubItemIcon />}>
                      {subItem.link ? (
                        <MenuLink title={subItem.title} link={subItem.link} />
                      ) : (
                        <span>{subItem.title}</span>
                      )}
                    </Menu.Item>
                  );
                }
              )}
            </Menu.SubMenu>
          );
        })}
      </Menu>
      <div
        className="navbar-footer"
        style={{
          flexDirection: collapsed ? "column" : "row",
        }}
      >
        <PoweroffOutlined
          className="trigger"
          style={{ margin: "auto" }}
          onClick={onLoggedOut}
        />
        <div>
          {!collapsed && <span>접기</span>}
          {React.createElement(
            collapsed ? AllIcons.MenuUnfoldOutlined : AllIcons.MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </div>
      </div>
    </Layout.Sider>
  );
};

export default SideNavBar;

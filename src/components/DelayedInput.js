import React, { useState } from "react";
import { Input } from "antd";

const DelayedInput = (props) => {
  const { onChange, delay, inputRef, valueFilter } = props;
  const [value, setValue] = useState();
  const [timeBreak, setTimeBreak] = useState(0);

  const handleChange = ({ target }) => {
    if (timeBreak) {
      clearTimeout(timeBreak);
    }

    let inputValue = target.value;

    if (inputValue && valueFilter) {
      inputValue = valueFilter(inputValue);
    }

    setValue(inputValue);
    setTimeBreak(setTimeout(() => onChange(inputValue), delay));
  };

  return (
    <Input {...props} value={value} ref={inputRef} onChange={handleChange} />
  );
};

export default DelayedInput;

import React from "react";
import { Modal, Typography } from "antd";

const ConfirmModal = ({
  open,
  title = "확인",
  text,
  onConfirm,
  onConfirmProps,
  onCancel,
}) => (
  <Modal
    title={title}
    visible={open}
    onOk={onConfirm}
    okButtonProps={onConfirmProps}
    onCancel={onCancel}
    okText="확인"
    cancelText="취소"
  >
    <Typography.Title level={4}>{text}</Typography.Title>
  </Modal>
);

export default ConfirmModal;

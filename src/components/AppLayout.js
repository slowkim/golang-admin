import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./AppLayout.css";
import { Breadcrumb, Divider, Layout, Space, Spin } from "antd";
import MainRouter from "../pages/Router";
import SideNavBar from "./SideNavBar";
import { useAuth } from "../context/AuthContext";
import Login from "../pages/Login";
import { useLoading } from "../context/LoadingContext";

const AppLayout = () => {
  const auth = useAuth();
  const loading = useLoading();
  const [authenticated, setAuthenticated] = useState(false);
  const history = useHistory();
  const [menuDepth, setMenuDepth] = useState([]);

  const checkAuth = () => {
    setAuthenticated(auth.checkAuth());
  };

  const signIn = () => {
    const from = history.location.state
      ? history.location.state.from
      : history.location || { pathname: "/" };
    history.replace(from);
    checkAuth();
  };

  const signOut = () => {
    auth.signOut();
    history.replace("/", { from: history.location });
    checkAuth();
  };

  useEffect(checkAuth, []);

  if (!authenticated) {
    return <Login onLoggedIn={signIn} />;
  }

  return (
    <Layout
      style={{
        height: "100%",
        overflow: "hidden",
      }}
    >
      <SideNavBar
        onMenuClick={setMenuDepth}
        userPermission={auth.getPermissions()}
        onLoggedOut={signOut}
      />
      <Layout className="site-layout">
        <Layout.Content className="site-layout-background">
          <Space
            className="loading-area"
            style={{
              zIndex: loading.status ? 999 : -999,
            }}
          >
            <Spin className="loading-icon" size="large" />
          </Space>
          <div style={{ minHeight: "86vh" }}>
            {menuDepth && menuDepth.length > 0 && (
              <div style={{ padding: "1rem" }}>
                <Breadcrumb>
                  {menuDepth.map((menu, i) => (
                    <Breadcrumb.Item key={i}>{menu.title}</Breadcrumb.Item>
                  ))}
                </Breadcrumb>
                <Divider />
              </div>
            )}
            <MainRouter />
          </div>
          <div className="site-layout-footer">
            <pre>
              어드민
              <br />
              &copy;2021 Created by slowkim
            </pre>
          </div>
        </Layout.Content>
      </Layout>
    </Layout>
  );
};
export default AppLayout;

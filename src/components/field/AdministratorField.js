import React, { useEffect, useState, memo } from "react";
import { Progress } from "antd";
import { useFetch } from "../../context/FetchContext";

const AdministratorField = ({ id, name }) => {
  const [admin, setAdmin] = useState();
  const fetch = useFetch();

  useEffect(() => {
    if (!id) {
      return;
    }

    if (admin) {
      return;
    }

    fetch
      .getOne(`admin/administrators/${id}`)
      .then(setAdmin)
      .catch(console.error);
  }, [fetch, id]);

  if (!admin) {
    return <Progress percent={100} status="active" showInfo={false} />;
  }

  return <span>{admin[name]}</span>;
};

export default memo(AdministratorField);

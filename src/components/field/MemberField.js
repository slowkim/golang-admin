import React, { useEffect, useState } from "react";
import { Progress } from "antd";
import { useFetch } from "../../context/FetchContext";

const MemberField = ({ id, name }) => {
  const [member, setMember] = useState();
  const fetch = useFetch();
  
  useEffect(() => {
    fetch.getOne(`admin/members/${id}`).then(setMember).catch(console.error);
  }, []);

  if (!member) {
    return <Progress percent={100} status="active" showInfo={false} />;
  }

  return <span>{member[name]}</span>;
};

export default MemberField;

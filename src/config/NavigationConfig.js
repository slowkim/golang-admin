import navigationInfo from "./navigation.json";

/* Link 가 있을 경우, 해당 Item을 result로 리턴
/* Link 가 없을 경우, 하위 Item을 result로 리턴 (Link가 있을 때까지 하위 접근) */
const searchPath = (items, path, userPermission) => {
  return searchPermitted(items, userPermission)
    .map((item) => {
      let result = [];

      if (item.link && item.link === path) {
        result.push(item);
      } else if (item.items) {
        // link가 없고 items가 있는 경우
        const matchedChild = searchPath(item.items, path, userPermission);
        if (matchedChild && matchedChild.length > 0) {
          result = [item, ...matchedChild];
        }
      }

      return result;
    })
    .filter((result) => result.length > 0)
    .pop();
};

const searchPermitted = (items = [], userPermission = []) => {
  return items.filter(
    (item) =>
      userPermission.filter((role) => item.permission.includes(role)).length >
      0
  );
};

export const NavigationConfig = {
  /* Items : json
  /* FindItem : path에 해당하는 Item 리턴 */
  Items: navigationInfo.items,
  FilterPermitted: searchPermitted,
  FindItem: (path, userPermission) =>
    navigationInfo.items
      .map((item) => {
        const result = searchPath(item.items, path, userPermission);
        if (result) {
          return [item, ...result];
        }
        return null;
      })
      .filter((item) => Boolean(item))
      .pop(),
};

import { fetchUtils } from "react-admin";
import qs from "qs";

const apiUrl = process.env.REACT_APP_API_HOST;
const httpClient = fetchUtils.fetchJson;

const createQuery = (param) => qs.stringify(param, { arrayFormat: "repeat" });

const DataProvider = (authProvider) => {
  const createUser = () => 
  authProvider.getToken() ? {
    authenticated: true,
    token: "Bearer " + authProvider.getToken()
  } : {};

  return {
    getList: (resource, params) => {
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;
      const query = {
        sort: JSON.stringify([field, order]),
        page: page,
        pageSize: perPage,
        ...params.filter,
      };
      const url = `${apiUrl}/${resource}?${createQuery(query)}`;

      return httpClient(url, {
        user: createUser()
      }).then(({ json }) => ({
        data: (json && json.result) || [],
        total: (json && json.totalCount) || 0,
      }));
    },

    get: (resource, params) => {
      const url = `${apiUrl}/${resource}?${createQuery(params)}`;
      return httpClient(url, {
        user: createUser()
      }).then(({ json }) => ({ data: json }));
    },

    getOne: (resource, params) =>
      httpClient(`${apiUrl}/${resource}/${params.id}`, {
        user: createUser()
      }).then(({ json }) => ({
        data: json,
      })),

    getMany: (resource, params) => {
      const url = `${apiUrl}/${resource}?${createQuery(params)}`;
      return httpClient(url, {
        user: createUser()
      }).then(({ json }) => ({ data: json.result }));
    },

    getManyReference: (resource, params) => {
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;
      const query = {
        sort: JSON.stringify([field, order]),
        range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
        filter: JSON.stringify({
          ...params.filter,
          [params.target]: params.id,
        }),
      };
      const url = `${apiUrl}/${resource}?${createQuery(query)}`;
      return httpClient(url, {
        user: createUser()
      }).then(({ headers, json }) => ({
        data: json,
        total: parseInt(headers.get("content-range").split("/").pop(), 10),
      }));
    },

    create: (resource, params) =>
      httpClient(`${apiUrl}/${resource}`, {
        method: "POST",
        body: JSON.stringify(params.data),
        user: createUser()
      }).then(({ json }) => ({
        data: { ...params.data, id: json.id },
      })),

    post: (resource, params) =>
      httpClient(`${apiUrl}/${resource}`, {
        method: "POST",
        body: JSON.stringify(params),
        user: createUser()
      }),

    update: (resource, params) =>
      httpClient(`${apiUrl}/${resource}/${params.id}`, {
        method: "PUT",
        body: JSON.stringify(params.data),
        user: createUser()
      }).then(({ json }) => ({ data: json })),

    updateMany: () => {
      throw new Error("UpdateMany not Implemented");
    },

    delete: () => {
      throw new Error("Delete not Implemented");
    },
    deleteMany: () => {
      throw new Error("DeleteMany not Implemented");
    },
  };
};

export default DataProvider;

import React, { createContext, useState, useContext } from "react";

const LoadingContext = createContext({
  status: false,
  update: () => {},
});

export const useLoading = () => useContext(LoadingContext);

export const LoadingProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);

  return (
    <LoadingContext.Provider value={{ status: loading, update: setLoading }}>
      {children}
    </LoadingContext.Provider>
  );
};

export default LoadingContext;

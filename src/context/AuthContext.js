import React, { createContext } from "react";
import jwt_decode from "jwt-decode";
import { useContext } from "react";

const authStorageKey = "auth";

const afterSignIn = (response) =>
  sessionStorage.setItem(authStorageKey, response.token);
// Auth response 받는다.
const signOut = () => sessionStorage.removeItem(authStorageKey);
// Log out, Auth response 를 삭제한다.
const checkAuth = () => Boolean(sessionStorage.getItem(authStorageKey));
// authResponse 가 있는지 확인한다.
const getAuth = () => {
  if (!checkAuth) {
    return undefined;
  }

  return jwt_decode(sessionStorage.getItem(authStorageKey));
}
// authResponse 가 있으면 JSON 파싱해서 Auth 정보를 가져온다.
const getPermissions = () => (!checkAuth() ? [] : getAuth().roles.split(","));
// Permission을 auth 가 있으면, auth.roles을 가져온다.
const getToken = () => sessionStorage.getItem(authStorageKey);
// Permission을 auth 가 있으면, auth.token을 가져온다.

const authManager = () => {
  return {
    afterSignIn: afterSignIn,
    signOut: signOut,
    checkAuth: checkAuth,
    getAuth: getAuth,
    getPermissions: getPermissions,
    getToken: getToken,
  };
};

const AuthContext = createContext(authManager());

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  return (
    <AuthContext.Provider value={authManager()}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;

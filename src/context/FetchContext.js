import React, { createContext, useContext } from "react";
import qs from "qs";
import { useAuth } from "./AuthContext";
import LoadingContext from "./LoadingContext";
import { message } from "antd";

const host = process.env.REACT_APP_API_HOST;
const defaultPageSize = 10;

const parseValidationMessage = (validationMessage) => {
  const [field, tag] = validationMessage.split(":");
  const actualField =
    field.indexOf(".") === -1 ? field : field.substring(field.indexOf(".") + 1);
  const [rule, param] = tag.split("/");

  let message;
  switch (rule) {
    case "required":
      message = "필수 입력항목입니다.";
      break;
    case "email":
      message = "올바른 이메일 양식이 아닙니다.";
      break;
    case "url":
      message = "올바른 URL 양식이 아닙니다.";
      break;
    case "postNo":
      message = "올바른 우편번호 양식이 아닙니다.";
      break;
    case "telNo":
      message = "올바른 전화번호 양식이 아닙니다.";
      break;
    case "mobileNo":
      message = "올바른 휴대전화번호 양식이 아닙니다.";
      break;
    case "businessRegistrationNo":
      message = "올바른 사업자등록번호 양식이 아닙니다.";
      break;
    case "residentRegistrationNo":
      message = "올바른 주민등록번호 양식이 아닙니다.";
      break;
    case "numeric":
      message = "숫자값만 입력 가능합니다.";
      break;
    case "len":
      message = `${param}자로 입력되어야 합니다.`;
      break;
    case "lte":
      message = `${param + 1}자 이상 입력할 수 없습니다.`;
      break;
    case "gte":
      message = `${param}자 이상 입력해야 합니다.`;
      break;
    default:
      message = "Unknown Validation Error";
    // TODO : 우리가 사용하는 validation tag에 대한 case 추가.
  }

  return {
    field: actualField,
    message: message,
  };
};

const ErrorMessageComponent = ({ messages = [] }) => {
  return (
    <>
      {messages.map((m) => (
        <div>{m}</div>
      ))}
    </>
  );
};

const FetchAdapter = (setLoading, message) => {
  const authProvider = useAuth();

  const parseError = async (res) => {
    if (res.ok) {
      return res;
    }

    if (res.status === 401) {
      authProvider.signOut();
      return;
    }

    const error = await res.json();
    let errorMessage = error.message;
    if (res.status >= 500 || error.code >= 90000) {
      errorMessage = "시스템 오류입니다. ";
    } else if (res.status === 400 && error.code === 10007) {
      const errors = error.message.split("\n").map(parseValidationMessage);
      errorMessage = `${errors[0].field} : ${errors[0].message}`;
      throw new HttpError(errorMessage, res.status, {
        detail: errors,
        ...error,
      });
    }
    throw new HttpError(errorMessage, res.status, error);
  };

  const handleResponse = async (res) => {
    const contentType = res.headers.get("Content-Type");

    if (contentType && contentType.indexOf("application/json") > -1) {
      return await res.json();
    }

    return await res.text();
  };

  const handleError = (err) => {
    if (err.body.detail) {
      const errorMessage = err.body.detail.map(
        (detail) => `${detail.field} : ${detail.message}`
      );
      message.error(<ErrorMessageComponent messages={errorMessage} />, [1]);
    } else {
      message.error(err.message, [1]);
    }

    throw err;
  };

  const createHeader = (headers = {}) => {
    let defaultHeaders = {
      "Content-Type": "application/json",
    };

    if (authProvider.checkAuth()) {
      defaultHeaders.Authorization = "Bearer " + authProvider.getToken();
    }

    return {
      ...defaultHeaders,
      ...headers,
    };
  };

  return {
    getOne: (path, options = {}) => {
      setLoading(true);
      return fetch(`${host}/${path}`, { headers: createHeader() })
        .then(parseError)
        .then(handleResponse)
        .catch((err) => {
          if (options.onFailure) {
            options.onFailure(err);
            return;
          }

          handleError(err);
        })
        .finally(() => setLoading(false));
    },

    getList: (path, pageParam, searchParam = {}, options = {}) => {
      const { page, pageSize } = pageParam;
      const param = {
        page: page ? page : 0,
        pageSize: pageSize || defaultPageSize,
        ...searchParam,
      };
      const queryString = qs.stringify(param, {
        arrayFormat: "repeat",
        skipNulls: true,
      });
      setLoading(true);
      return fetch(`${host}/${path}?${queryString}`, {
        headers: createHeader(),
      })
        .then(parseError)
        .then(handleResponse)
        .then((data) => {
          return {
            result: data.result || [],
            ...data,
          };
        })
        .catch((err) => {
          if (options.onFailure) {
            options.onFailure(err);
            return;
          }

          handleError(err);
        })
        .finally(() => setLoading(false));
    },

    post: (path, body, options = {}) => {
      setLoading(true);

      return fetch(`${host}/${path}`, {
        method: "POST",
        headers: createHeader(),
        body: JSON.stringify(body),
      })
        .then(parseError)
        .then(handleResponse)
        .catch((err) => {
          if (options.onFailure) {
            options.onFailure(err);
            return;
          }

          handleError(err);
        })
        .finally(() => setLoading(false));
    },

    put: (path, body, options = {}) => {
      setLoading(true);
      return fetch(`${host}/${path}`, {
        method: "PUT",
        headers: createHeader(),
        body: JSON.stringify(body),
      })
        .then(parseError)
        .then(handleResponse)
        .catch((err) => {
          if (options.onFailure) {
            options.onFailure(err);
            return;
          }

          handleError(err);
        })
        .finally(() => setLoading(false));
    },

    upload: (filePath, files = [], options = {}) => {
      setLoading(true);

      const formData = new FormData();
      formData.append("path", filePath);

      files.map((f) => formData.append("files", f));

      return fetch(`${host}/files`, {
        method: "POST",
        headers: {
          Authorization: authProvider.getToken(),
        },
        body: formData,
      })
        .then(parseError)
        .then(handleResponse)
        .catch((err) => {
          if (options.onFailure) {
            options.onFailure(err);
            return;
          }

          handleError(err);
        })
        .finally(() => setLoading(false));
    },
  };
};

export const FetchContext = createContext(null);

export const useFetch = () => useContext(FetchContext);

export const FetchProvider = ({ children }) => {
  const loading = useContext(LoadingContext);
  const fetch = FetchAdapter(loading.update, message);

  return (
    <FetchContext.Provider value={fetch}>{children}</FetchContext.Provider>
  );
};

class HttpError extends Error {
  constructor(message, status, body) {
    super(message);
    Object.setPrototypeOf(this, HttpError.prototype);
    this.name = this.constructor.name;
    this.body = body;
    if (typeof Error.captureStackTrace === "function") {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = new Error(message).stack;
    }
    this.stack = new Error().stack;
  }
}

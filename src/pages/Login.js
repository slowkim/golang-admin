import React from "react";
import { Button, Form, Input, message, Typography } from "antd";
import { useAuth } from "../context/AuthContext";
import { useFetch } from "../context/FetchContext";
import { CircleLogo } from "../components/Logo";

const styles = {
  layout: {
    width: 320,
    margin: "100px auto",
  },
  fullWidth: {
    width: "100%",
  },
};

const Login = ({ onLoggedIn }) => {
  const fetch = useFetch();
  const auth = useAuth();

  const signIn = (values) => {
    fetch
      .post("auth/login", values, {
        onFailure: (err) => {
          if (err.body.code === 10008) {
            message.error("계정정보가 잘못되었습니다. 다시 확인해주세요.", [2]);
          } else {
            message.error(err.message, [2]);
          }
          throw err;
        },
      })
      .then(auth.afterSignIn)
      .then(onLoggedIn)
      .catch(console.error);
  };

  return (
    <Form
      name="login"
      style={styles.layout}
      labelCol={{ span: 10 }}
      wrapperCol={{ span: 30 }}
      onFinish={signIn}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-evenly",
          alignItems: "baseline",
        }}
      >
        <CircleLogo />
        <Typography.Title level={4}>Admin</Typography.Title>
      </div>

      <Form.Item name="email">
        <Input placeholder="Email" />
      </Form.Item>
      <Form.Item name="password">
        <Input.Password placeholder="Password" />
      </Form.Item>
      <Button style={styles.fullWidth} type="primary" htmlType="submit">
        로그인
      </Button>
    </Form>
  );
};

export default Login;

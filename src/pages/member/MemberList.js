import React, { useCallback, useEffect, useState } from "react";
import { Table } from "antd";
import { useFetch } from "../../context/FetchContext";
import moment from "moment";

const { Column } = Table;

const PAGE_SIZE = 10;

const MemberList = () => {
  const [data, setData] = useState({ totalCount: 0, result: [] });
  const fetch = useFetch();

  const load = useCallback((page, pageSize) => {
    fetch
      .getList("admin/members", {
        page: page,
        pageSize: pageSize,
      })
      .then((json) => setData(json));
  }, []);

  useEffect(() => {
    load();
  }, [load]);

  return (
    <>
      <div>총 {data.totalCount}건</div>
      <Table
        dataSource={data.result}
        rowKey="id"
        pagination={{
          pageSize: PAGE_SIZE,
          total: data.totalCount,
          onChange: load,
        }}
      >
        <Column title="회원번호" dataIndex="id" key="id" />
        <Column title="카카오 닉네임" dataIndex="nickname" key="nickname" />
        <Column title="연락처" dataIndex="mobile" key="mobile" />
        <Column
          title="카카오 프로필이미지"
          dataIndex="profileImageURL"
          key="profileImageURL"
          render={(text) => {
            return <img src={text} width="100" />;
          }}
        />
        <Column
          title="가입일자"
          dataIndex="createdAt"
          key="createdAt"
          render={(text) => {
            const localDateTime = moment(text)
              .local()
              .format("YYYY-MM-DD HH:mm:ss");
            return <span>{localDateTime}</span>;
          }}
        />
        <Column title="선택" />
      </Table>
    </>
  );
};

export default MemberList;
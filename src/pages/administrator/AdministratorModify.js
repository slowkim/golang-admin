import React, { useEffect, useState } from "react";
import { useFetch } from "../../context/FetchContext";
import {
  Button,
  Col,
  Form,
  Input,
  message,
  Row,
  Space,
  Typography,
} from "antd";
import { useAuth } from "../../context/AuthContext";

const { Title } = Typography;

const AdministratorModify = ({ history, match }) => {
  const fetch = useFetch();
  const id = match.params.id;
  const [form] = Form.useForm();
  const [memberInfo, setMemberInfo] = useState();
  const auth = useAuth();

  const validatePasswordCheck = (_, value) => {
    if (form.getFieldValue("password") === value) {
      return Promise.resolve();
    }
    return Promise.reject(new Error("비밀번호가 일치하지 않습니다"));
  };

  const load = () => {
    if (auth.getPermissions().includes("admin")) {
      fetch
        .getOne(`admin/administrators/${id}`)
        .then(setMemberInfo)
        .catch(console.error);
      return;
    }
  };

  const handleSubmit = (values) => {
    let requestBody = values;
    fetch
      .put(`admin/administrators/${id}`, {
        id: parseInt(id),
        roleType: "admin",
        ...requestBody,
      })
      .then(() => {
        message.success("성공적으로 수정되었습니다.", [3]);
        history.replace(`/administrator`);
      })
      .catch(console.error);
  };
  useEffect(() => {
    load();
  }, []);

  if (!memberInfo) {
    return null;
  }

  return (
    <>
      <Space
        style={{
          padding: "0.5rem",
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button
          style={{ backgroundColor: "#dde0e5" }}
          onClick={() => history.replace(`/administrator`)}
        >
          목록으로
        </Button>
      </Space>
      <Form
        form={form}
        size="large"
        style={{ maxWidth: 600, margin: "15px" }}
        onFinish={handleSubmit}
        initialValues={memberInfo}
      >
        <Title level={3}> 계정 수정</Title>
        <Row>
          <Col xs={8} sm={10} md={10}>
            <Typography style={{ fontSize: "2rem" }}>이메일</Typography>
          </Col>
          <Col xs={16} sm={14} md={14}>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "필수값입니다.",
                },
                {
                  pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                  message: "올바른 이메일 양식이 아닙니다.",
                },
              ]}
            >
              <Input
                style={{
                  width: "100%",
                  backgroundColor: "#c7d5eb",
                  border: "1px solid #4b9927",
                }}
                disabled
                autoComplete="new-password"
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col xs={8} sm={10} md={10}>
            <Typography style={{ fontSize: "2rem" }}>비밀번호</Typography>
          </Col>
          <Col xs={16} sm={14} md={14}>
            <Form.Item
              name="password"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "필수값입니다.",
                },
                {
                  min: 6,
                  message: "비밀번호는 6자리 이상 입력해 주세요",
                },
              ]}
            >
              <Input.Password
                style={{
                  width: "100%",
                  backgroundColor: "#c7d5eb",
                  border: "1px solid #4b9927",
                }}
                autoComplete="new-password"
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col xs={8} sm={10} md={10}>
            <Typography style={{ fontSize: "2rem" }}>비밀번호 확인</Typography>
          </Col>
          <Col xs={16} sm={14} md={14}>
            <Form.Item
              name="confirmPassword"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "필수값입니다.",
                },
                {
                  min: 6,
                  message: "비밀번호를 6자리 이상 입력해 주세요",
                },
                {
                  validator: validatePasswordCheck,
                },
              ]}
            >
              <Input.Password
                style={{
                  width: "100%",
                  backgroundColor: "#c7d5eb",
                  border: "1px solid #4b9927",
                }}
                autoComplete="new-password"
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col xs={8} sm={10} md={10}>
            <Typography style={{ fontSize: "2rem" }}>휴대전화</Typography>
          </Col>
          <Col xs={16} sm={14} md={14}>
            <Form.Item
              name="mobile"
              rules={[
                {
                  required: true,
                  message: "필수값입니다.",
                },
                {
                  pattern: /^[0-9]*$/,
                  message: "숫자만 입력 가능합니다.",
                },
                {
                  len: 11,
                  message: "휴대전화 11자리를 입력해 주세요",
                },
              ]}
            >
              <Input
                style={{
                  width: "100%",
                  backgroundColor: "#c7d5eb",
                  border: "1px solid #4b9927",
                }}
                autoComplete="off"
              />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item shouldUpdate>
          {() => (
            <Button
              type="primary"
              htmlType="submit"
              style={{ width: "100%" }}
              disabled={
                !Boolean(memberInfo) ||
                !!form.getFieldsError().filter(({ errors }) => errors.length)
                  .length
              }
            >
              수정
            </Button>
          )}
        </Form.Item>
      </Form>
    </>
  );
};
export default AdministratorModify;

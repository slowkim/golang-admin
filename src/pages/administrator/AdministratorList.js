import React, { useCallback, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Button, Space, Table, Tag } from "antd";
import { useFetch } from "../../context/FetchContext";
import moment from "moment";
import AdministratorField from "../../components/field/AdministratorField";
import { useAuth } from "../../context/AuthContext";
import { EditOutlined } from "@ant-design/icons";

const { Column } = Table;

const PAGE_SIZE = 10;

const AdministratorList = () => {
  const [data, setData] = useState({ totalCount: 0, result: [] });
  const fetch = useFetch();
  const auth = useAuth();
  const history = useHistory();
  const id = auth.getAuth().id;

  const load = useCallback((page, pageSize) => {
    let searchParam;

    fetch
      .getOne(`admin/administrators/${id}`)
      .then((res) => {
        if (!auth.getPermissions().includes("admin")) {
          searchParam = {
            storeId: res.storeId,
          };
        }

        fetch
          .getList(
            "admin/administrators",
            {
              page: page,
              pageSize: pageSize,
            },
            searchParam
          )
          .then(setData)
          .catch(console.error);
      })
      .catch(console.error);
  }, []);

  useEffect(() => {
    load();
  }, [load]);

  return (
    <>
      <Space
        style={{
          padding: "0.5rem",
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button
          style={{ backgroundColor: "#dde0e5" }}
          onClick={() => history.replace(`/administrator/register`)}
        >
          계정 생성
        </Button>
      </Space>
      <div>총 {data.totalCount}건</div>
      <Table
        dataSource={data.result}
        rowKey="id"
        pagination={{
          pageSize: PAGE_SIZE,
          total: data.totalCount,
          onChange: load,
        }}
      >
        <Column title="ID" dataIndex="id" key="id" width="100px" />
        <Column title="이메일" dataIndex="email" key="email" width="200px" />
        {auth.getPermissions().includes("admin") && (
          <Column
            title="휴대전화"
            dataIndex="mobile"
            key="mobile"
            width="150px"
          />
        )}
        <Column
          title="권한"
          dataIndex="roles"
          key="roles"
          render={(text) => (
            <>
              {text.map((role) => (
                <Tag key={role} color="blue">
                  {role}
                </Tag>
              ))}
            </>
          )}
        />
        <Column
          title="등록일시"
          dataIndex="createdAt"
          key="createdAt"
          width="200px"
          render={(record) =>
            moment(record).local().format("YYYY-MM-DD HH:mm:ss")
          }
        />
        <Column
          width="40px"
          render={(record) => {
            return (
              <Link to={`/administrator/${record.id}/modify`}>
                <Button type="primary" icon={<EditOutlined />} />
              </Link>
            );
          }}
        />
      </Table>
    </>
  );
};

export default AdministratorList;

import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { useAuth } from "../context/AuthContext";
import AdministratorList from "./administrator/AdministratorList";
import AdministratorRegister from "./administrator/AdministratorRegister";
import AdministratorModify from "./administrator/AdministratorModify";
import MemberList from "./member/MemberList";

const MainPage = () => {
  const auth = useAuth();

  const mainPagePath = () => {
    const permissions = auth.getPermissions();

    if (permissions.includes("admin")) {
      return "/administrators";
    } else if (permissions.includes("store_manager")) {
      return "/donation";
    } else {
      return "/donation-item";
    }
  };

  return <Redirect to={mainPagePath()} />;
};

const MainRouter = () => (
  <Switch>
    <AuthorizedRoute
      exact
      permission={["admin"]}
      path="/administrator"
      component={AdministratorList}
    />
    <AuthorizedRoute
      exact
      permission={["admin"]}
      path="/administrator/register"
      component={AdministratorRegister}
    />
    <AuthorizedRoute
      exact
      permission={["admin"]}
      path="/administrator/:id/modify"
      component={AdministratorModify}
    />
    <AuthorizedRoute
      exact
      permission={["admin"]}
      path="/member"
      component={MemberList}
    />
    <Route exact path="/">
      <MainPage />
    </Route>
  </Switch>
);

const AuthorizedRoute = (props) => {
  const { permission } = props || { permission: [] };
  const auth = useAuth();

  const allowed = auth
    .getPermissions()
    .filter((role) => permission.includes(role));

  if (allowed.length === 0) {
    return <MainPage />;
  }
  return <Route {...props} />;
};

export default MainRouter;

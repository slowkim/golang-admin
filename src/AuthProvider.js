import { fetchUtils } from "react-admin";

const httpClient = fetchUtils.fetchJson;

const AuthProvider = {
  login: ({ username, password }) =>
    httpClient(`${process.env.REACT_APP_API_HOST}/accounts/login`, {
      method: "POST",
      body: JSON.stringify({
        login: username,
        password: password,
      }),
    })
      .then((res) => res.json)
      .then((result) => sessionStorage.setItem("auth", result))
      .catch((err) => err.message),
  logout: () => {
    sessionStorage.removeItem("auth");
    return Promise.resolve();
  },
  checkAuth: () =>
    sessionStorage.getItem("auth")
      ? Promise.resolve()
      : Promise.reject({ redirectTo: "/login" }),
  checkError: (error) =>
    error.status !== 401 && error.status !== 403
      ? Promise.resolve()
      : Promise.reject({ redirectTo: "/login" }),
  getPermissions: () =>
    sessionStorage.getItem("auth")
      ? Promise.resolve(sessionStorage.getItem("auth"))
          .then(JSON.parse)
          .then((auth) => auth.roles)
      : Promise.resolve([]),
  getToken: () =>
    sessionStorage.getItem("auth")
      ? JSON.parse(sessionStorage.getItem("auth")).token
      : "",
};

export default AuthProvider;
